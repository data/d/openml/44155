# OpenML dataset: credit-approval_reproduced

https://www.openml.org/d/44155

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Confidential - Donated by Ross Quinlan   
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/credit+approval) - 1987  
**Please cite**: [UCI](http://archive.ics.uci.edu/ml/citation_policy.html)  

**Credit Approval**
This file concerns credit card applications. All attribute names and values have been changed to meaningless symbols to protect the confidentiality of the data.  
   
This dataset is interesting because there is a good mix of attributes -- continuous, nominal with small numbers of values, and nominal with larger numbers of values.  There are also a few missing values.

 From OpenML: https://www.openml.org/d/29

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44155) of an [OpenML dataset](https://www.openml.org/d/44155). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44155/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44155/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44155/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

